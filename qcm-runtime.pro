TEMPLATE = lib
TARGET = $$qtLibraryTarget(qcm-runtime)

HEADERS += qcmobjecthome.h

INSTALL_PREFIX = $$(QCM_INSTALL)
isEmpty(INSTALL_PREFIX) {
    INSTALL_PREFIX = /usr
}

target.path = $$INSTALL_PREFIX/lib/

header_files.files = $$HEADERS
header_files.path = $$INSTALL_PREFIX/include/qcm/

INSTALLS += target header_files
